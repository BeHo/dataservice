package dataService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

import dataService.config.DbType;
import dataService.config.H2EncryptionModes;
import dataService.config.JdbcDataTypes;
import dataService.queryBuilder.CreateSchemaQueryBuilder;
import dataService.queryBuilder.CreateTableQueryBuilder;
import dataService.queryBuilder.DeleteQueryBuilder;
import dataService.queryBuilder.InsertQueryBuilder;
import dataService.queryBuilder.SelectQueryBuilder;
import dataService.queryBuilder.UpdateQueryBuilder;
import dataService.statementExecuter.CreateStatementExecuter;
import dataService.statementExecuter.SelectStatementExecuter;
import dataService.statementExecuter.UpdateStatementExecuter;
import dataService.syntaxComposer.DdlSyntaxComposer;
import dataService.syntaxComposer.DmlSyntaxComposer;
import dataService.syntaxComposer.DqlSyntaxComposer;

public class ComponentDao {

	private static ComponentDao instance = null;
	private static boolean isInitialized;
	private static Connection connection;
	private static DbType dbSystem;
	private static String username;

	private ComponentDao() {
		super();
	}

	/**
	 * Init used for following database types: MySQL, PostgreSQL, Maria DB. The init
	 * method guaratees that a correctly constructed connection string is available
	 * once the single instance of this class is requested.
	 * 
	 * @param driver
	 * @param dbType
	 * @param hostName
	 * @param database
	 */
	public static void init(String user, String password, String driver, DbType dbType, String hostName,
			String database) {
		dbSystem = dbType;
		username = user;
		StringBuilder builder = new StringBuilder();
		builder.append(driver);
		builder.append(":");
		builder.append(dbType);
		builder.append("://");
		builder.append(hostName);
		builder.append("/");
		builder.append(database);
		establishConnection(builder.toString(), user, password);
	}

	/**
	 * Init including port parameter; used for following database types: MySQL,
	 * PostgreSQL, Maria DB. The init method guarantees that a correctly constructed
	 * connection string is available once the single instance of this class is
	 * requested.
	 * 
	 * @param driver
	 * @param dbType
	 * @param port
	 * @param hostName
	 * @param database
	 */
	public static void init(String user, String password, String driver, DbType dbType, String port, String hostName,
			String database) {
		dbSystem = dbType;
		username = user;
		StringBuilder builder = new StringBuilder();
		builder.append(driver);
		builder.append(":");
		builder.append(dbType);
		builder.append("://");
		builder.append(hostName);
		builder.append(":");
		builder.append(port);
		builder.append("/");
		builder.append(database);
		establishConnection(builder.toString(), user, password);
	}

	/**
	 * Init used for SQLite databases. The init method guarantees that a correctly
	 * constructed connection string is available once the single instance of this
	 * class is requested.
	 * 
	 * @param driver
	 * @param dbPath
	 */
	public static void initSqlite(String driver, String dbPath) {
		dbSystem = DbType.SQLITE;
		StringBuilder builder = new StringBuilder();
		builder.append(driver);
		builder.append(":sqlite:");
		builder.append(dbPath);
		establishConnection(builder.toString());
	}

	/**
	 * Init used to connect to H2 databases. While versatile, H2 is supported as an
	 * embedded local database here only (at least currently). That means no
	 * connections to external servers are possible right now. Note that password
	 * and username have to be provided (meaning that unencrypted databases are not
	 * supported)!
	 */

	public static void initH2(String password, String driver, String user, H2EncryptionModes cipher, String dbPath) {
		dbSystem = DbType.H2;
		username = user;
		StringBuilder builder = new StringBuilder();
		builder.append(driver);
		builder.append(":h2:");
		builder.append(dbPath);
		builder.append(";CIPHER=");
		builder.append(cipher);
		establishConnection(builder.toString(), user, password);
	}

	/**
	 * Init used for MS SQL Server. A String of property-value pairs must be added
	 * at the end, at least containing a valid database name. If this String is not
	 * formatted correctly or the database name is invalid, the method fails. The
	 * init method guarantees that a correctly constructed connection string is
	 * available once the single instance of this class is requested.
	 * 
	 * @param driver
	 * @param dbType
	 * @param hostName
	 */
	public static void initSqlServer(String user, String password, String driver, String hostName, String properties) {
		dbSystem = DbType.SQLSERVER;
		username = user;
		StringBuilder builder = new StringBuilder();
		builder.append(driver);
		builder.append(":sqlserver://");
		builder.append(hostName);
		builder.append(";");
		builder.append(properties);
		establishConnection(builder.toString(), user, password);
	}

	/**
	 * Full init used for MS SQL Server (adding port). <b>Important:
	 * </b>Property/value pairs need to be well formatted, or the method will likely
	 * crash. The init method guarantees that a correctly constructed connection
	 * string is available once the single instance of this class is requested.
	 * 
	 * @param driver
	 * @param dbType
	 * @param hostName
	 */
	public static void initSqlServer(String user, String password, String driver, String hostName, String port,
			String properties) {
		dbSystem = DbType.SQLSERVER;
		username = user;
		StringBuilder builder = new StringBuilder();
		builder.append(driver);
		builder.append(":sqlserver://");
		builder.append(hostName);
		builder.append(";");
		builder.append(port);
		builder.append("/");
		builder.append(properties);
		establishConnection(builder.toString(), user, password);
	}

	private static void establishConnection(String connString, String user, String password) {
		try {
			connection = DriverManager.getConnection(connString, user, password);
			isInitialized = true;
			System.out.println("Successfully connected to database. Details:\n" + connString);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void establishConnection(String connString) {
		try {
			connection = DriverManager.getConnection(connString);
			isInitialized = true;
			System.out.println("Successfully connected to database. Details:\n" + connString);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static ComponentDao getInstance() {
		if (instance == null) {
			if (!isInitialized) {
				throw new IllegalStateException("DAO class not initialized - always call init() before getInstance()!");
			} else {
				instance = new ComponentDao();
			}
		}
		return instance;
	}

	// TODO write a no-mapper method which can be called if no Java object mapping
	// is required or intended. This could especially be useful in case of joins
	// (which would require special POJOs for each variant join).

	public ArrayList<Object> readValue(Object mapperObject, DqlSyntaxComposer syntax, JdbcDataTypes[]... types) {
		SelectQueryBuilder qBuilder = new SelectQueryBuilder(syntax);
		qBuilder.buildString();
		SelectStatementExecuter executer = new SelectStatementExecuter(connection, qBuilder, mapperObject);
		if (types.length > 0) {
			executer.setTypes(types[0]);
		}
		executer.buildStatement();
		executer.executeStatement();
		return executer.getResults();
	}

	public int deleteEntry(DmlSyntaxComposer syntax, JdbcDataTypes[] types) {
		DeleteQueryBuilder qBuilder = new DeleteQueryBuilder(syntax);
		qBuilder.buildString();
		UpdateStatementExecuter executer = new UpdateStatementExecuter(connection, qBuilder);
		executer.setTypes(types);
		executer.buildStatement();
		executer.executeStatement();
		return executer.getAffectedRows();
	}

	public ArrayList<Integer> insertEntry(DmlSyntaxComposer syntax, JdbcDataTypes[] types) {
		InsertQueryBuilder qBuilder = new InsertQueryBuilder(syntax);
		qBuilder.buildString();
		UpdateStatementExecuter executer = new UpdateStatementExecuter(connection, qBuilder);
		executer.setTypes(types);
		executer.buildStatement();
		executer.executeStatement();
		return executer.getInsertedIds();
	}

	public int updateEntry(DmlSyntaxComposer syntax, JdbcDataTypes[] types) {
		UpdateQueryBuilder qBuilder = new UpdateQueryBuilder(syntax);
		qBuilder.buildString();
		UpdateStatementExecuter executer = new UpdateStatementExecuter(connection, qBuilder);
		executer.setTypes(types);
		executer.buildStatement();
		executer.executeStatement();
		return executer.getAffectedRows();
	}

	/**
	 * Creates a new schema (or database, in case of MySQL). SQLite does not support
	 * the schema concept, so calls using such a database won't do anything. Note
	 * that in MySql databses, the schema/database is always created for the user
	 * who is currently logged in: the parameter 'user' is not used in this case and
	 * can be empty or null. In case of H2, PostgreSQL ad MS Server databases, the
	 * user must be specified.
	 */
	public void createSchema(DdlSyntaxComposer syntax) {
		if (DbType.SQLITE.equals(dbSystem)) {
			System.out.println("DB type SQLITE does not support schemata.");
		} else {
			syntax.setUser(username);
			syntax.setDbType(dbSystem);
			CreateSchemaQueryBuilder schemaBuilder = new CreateSchemaQueryBuilder(syntax);
			schemaBuilder.buildString();
			CreateStatementExecuter executer = new CreateStatementExecuter(connection, schemaBuilder);
			executer.executeStatement();
		}
	}

	public void createTable(DdlSyntaxComposer syntax) {
		CreateTableQueryBuilder tableBuilder = new CreateTableQueryBuilder(syntax);
		tableBuilder.buildString();
		CreateStatementExecuter executer = new CreateStatementExecuter(connection, tableBuilder);
		executer.executeStatement();
	}
}
