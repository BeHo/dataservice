package dataService;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;

public class ResultSetMapper {

	private ResultSet rs;
	private Object targetObject;

	/**
	 * Maps result set values to a list of specified objects by parsing
	 * <i>targetObject</i>'s setter methods. The mapper therefore parses
	 * <i>targetObject</i>, extracts such and only such methods that start with
	 * either 'set' or 'is' and calls them on <i>targetObject</i>. The class'
	 * <i>mapToObject</i> method is meant as a central point of access; it returns
	 * an ArrayList of mapped versions of the Object passed as a parameter before.
	 * 
	 * @param rs           - ResultSet; data as obtained by jdbc
	 * @param targetObject - Object; the Object that should be mapped.
	 */
	public ResultSetMapper(ResultSet rs, Object targetObject) {
		super();
		this.rs = rs;
		this.targetObject = targetObject;
	}

	/**
	 * Public access point. Call this to start mapping.
	 * 
	 * @return ArrayList - a list of Objects, mapped versions of the same object
	 *         previously passed as parm.
	 */
	public ArrayList<Object> mapToObject() {
		ArrayList<Object> rv = new ArrayList<>(); // consider this the complete result set
		try {

			// map every single result set (rs.next()) to one (new) instance of
			// targetObject's type
			while (rs.next()) {
				Object resultSetRow = Class.forName(targetObject.getClass().getName()).getDeclaredConstructor()
						.newInstance();
				ResultSetMetaData meta = rs.getMetaData();
				for (int i = 0; i < meta.getColumnCount(); i++) {
					Object value = rs.getObject(meta.getColumnName(i + 1));
					Method m = getSetterMethod(meta.getColumnName(i + 1));
					m.setAccessible(true);
					resultSetRow = invokeMethod(resultSetRow, m, value);
				}
				rv.add(resultSetRow);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rv;
	}

	/**
	 * This actually calls the passed setter method <i>m</i>, which is part of
	 * <i>currentObject</i> using <i>parmValue</i> as parameter. This rather wordy
	 * method is necessary due to the fact that java.lang.refelct.Method's invoke()
	 * function takes Objects as arguments and cannot handle primitive types. As a
	 * consequence, any primitive type that comes with <i>parmValue</i> has to be
	 * autoboxed individually. <b>Important: </b> <u>this method will abort if more
	 * then one parameter is detected in the given setter method!</u> Hence, this
	 * mapper only works for 'classic' setters accepting only the value required to
	 * set the respective field.
	 * 
	 * @param m             - java.lang.Method; the (setter) method to call
	 * @param parmValue     - Object; the parameter value to pass to setter.
	 * @param currentObject - Object; the object on which method m should be called.
	 *                      This is actually a new instance of the class attribute
	 *                      <i>targetObject</i>
	 */
	private Object invokeMethod(Object currentObject, Method m, Object parmValue) {
		Class<?>[] parms = m.getParameterTypes();
		// We don't know what the user coded, so setters with multiple parameters are
		// possible => abort in that case
		try {
			if (parms.length > 1) {
				throw new Exception();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Illegal number of Parameters for setter method, must have exactly 1!");
			e.printStackTrace();
		}
		try {

			// Handle primitive types manually, Method.invoke() cannot handle primitive
			// types!

			// INTEGER
			if ("int".equals(parms[0].getName())) {
				Integer castedValue = (Integer) parmValue;
				m.invoke(currentObject, castedValue);
			}

			// BOOLEAN
			// Expects Strings "1" or "0" and converts to Bool.
			else if ("boolean".equals(parms[0].getName())) {
				Boolean castedValue = convertToBool(parmValue);
				m.invoke(currentObject, castedValue);
			}

			// LONG
			else if ("long".equals(parms[0].getName())) {
				Long castedValue = (Long) parmValue;
				m.invoke(currentObject, castedValue);
			}

			// SHORT
			else if ("short".equals(parms[0].getName())) {
				Short castedValue = (Short) parmValue;
				m.invoke(currentObject, castedValue);
			}

			// DOUBLE
			else if ("double".equals(parms[0].getName())) {
				Double castedValue = (Double) parmValue;
				m.invoke(currentObject, castedValue);
			}

			// FLOAT
			else if ("float".equals(parms[0].getName())) {
				Float castedValue = (Float) parmValue;
				m.invoke(currentObject, castedValue);
			}

			// REAL
			else if ("real".equals(parms[0].getName())) {
				Float castedValue = (Float) parmValue;
				m.invoke(currentObject, castedValue);
			}

			// CHARACTER
			else if ("char".equals(parms[0].getName())) {
				Character castedValue = (Character) parmValue;
				m.invoke(currentObject, castedValue);
			}

			// otherwise it's probably an object
			else {
				m.invoke(currentObject, m.getParameterTypes()[0].cast(parmValue));
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return currentObject;
	}

	private Boolean convertToBool(Object parmValue) {
		Boolean rv = false;
		if ("1".equals((String) parmValue)) {
			rv = true;
		}
		return rv;
	}

	/**
	 * Helper method removing prefixes 'set' and 'is' from setters.
	 * 
	 * @param name
	 * @return The method name (aka the field name to be set) bare of setter
	 *         prefixes.
	 */
	private String stripMethodName(String name) {
		String rv = null;
		if (name.startsWith("set")) {
			rv = name.substring(3); 
		} else if (name.startsWith("is")) {
			rv = name.substring(2);
		} else {
			rv = "";
		}
		return rv;
	}

	/**
	 * Helper method: strips column names in case functions (such as MAX(), MIN()
	 * etc.) have been used. Columns names carry the function's title in these cases
	 * (e.g. MAX(column1)), which destroys mapping.
	 * 
	 * @param name
	 */
	private String stripColumnName(String name) {
		String rv = name;
		if (name.contains("(")) {
			rv = name.replaceAll(".*\\(", "");
			rv = rv.replace(")", "");
		}
		return rv;
	}

	/**
	 * Helper method to extract just setters from <i>targetObject</i>'s methods.
	 * 
	 * @return ArrayList of Method Objects stripped of any non-setter methods
	 */
	private Method getSetterMethod(String name) {

		name = stripColumnName(name);
		// Extract setter methods by checking their beginning
		for (Method m : targetObject.getClass().getDeclaredMethods()) {
			String strippedMethodName = stripMethodName(m.getName());
			if (!"".equals(strippedMethodName)) {
				if (name.equalsIgnoreCase(strippedMethodName)) {
					return m;
				}
			}
		}
		return null;
	}
}
