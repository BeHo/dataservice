package dataService.config;

public enum H2EncryptionModes {
	AES, XTEA, FOG
}
