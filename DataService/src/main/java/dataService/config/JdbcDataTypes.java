package dataService.config;

/**
 * List of data types recognized by JDBC for setting variables in prepared
 * statements. Use this for automatic parsing and method calling. The type's
 * names are deliberately not following syntax rules, because these values are
 * used as parts of method names in Reflection constructs. THis wouldn't work
 * with uppercase names.
 * 
 * @author Bene
 *
 */
public enum JdbcDataTypes {
	Int, Double, Float, Long, String, Date, Timestamp, Datetime, Boolean
}
