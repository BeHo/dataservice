package dataService.config;

public enum SqlDataTypes {
	INT, SMALLINT, BIGINT, DECIMAL, FLOAT, REAL, VARCHAR, CHAR, DATE, TIME, TIMESTAMP, BLOB
}
