package dataService.config;

import java.io.IOException;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class SqlDataTypesAdapter extends XmlAdapter<String, SqlDataTypes> {

	@Override
	public SqlDataTypes unmarshal(String value) throws IOException {
		SqlDataTypes rv = null;
		for (SqlDataTypes type : SqlDataTypes.values()) {
			if (value.equalsIgnoreCase(type.toString())) {
				rv = type;
			}
		}
		// If none of the enum SQL types matches the given value, rv is still null =>
		// probably the data type was declared incorrectly in the XML file
		if (rv == null) {
			throw new IOException(
					"Couldn't map value " + value + " from XML input to SQL data type. Check your XML declaration!");
		}
		return rv;
	}

	@Override
	public String marshal(SqlDataTypes value) throws Exception {
		return value.toString();
	}

}
