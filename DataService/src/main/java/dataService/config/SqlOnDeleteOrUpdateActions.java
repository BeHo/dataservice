package dataService.config;

public enum SqlOnDeleteOrUpdateActions {
	CASCADE, DELETE, RESTRICT, SET_NULL, NO_ACTION, SET_DEFAULT 
}
