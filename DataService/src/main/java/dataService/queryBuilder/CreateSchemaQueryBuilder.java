package dataService.queryBuilder;

import dataService.config.DbType;
import dataService.syntaxComposer.DdlSyntaxComposer;

public class CreateSchemaQueryBuilder extends QueryBuilder {

	private StringBuilder builder;
	private DdlSyntaxComposer syntax;

	public CreateSchemaQueryBuilder(DdlSyntaxComposer syntax) {
		this.builder = new StringBuilder();
		this.syntax = syntax;
	}

	public void buildString() {
		String schemaName = clearParameters();
		builder.append("CREATE SCHEMA ");

		// Exclude MS SQL server from IF NOT EXISTS clause
		// This is supported, but in a completely different way => I won't bother with
		// that. SQL server implementation is quite shallow anyway
		if (!syntax.getDbType().equals(DbType.SQLSERVER)) {
			builder.append("IF NOT EXISTS ");
		}
		builder.append(schemaName);
		if (!DbType.MYSQL.equals(syntax.getDbType())) {
			builder.append(" AUTHORIZATION ");
			builder.append(syntax.getUser());
		}
		this.finalQuery = builder.toString();
	}

	/**
	 * Helper method to strip the intended schema name of any special characters
	 * that might be used for SQL injection, such as quotation marks, semicolon and
	 * whitespace.
	 * 
	 * @return
	 */
	private String clearParameters() {
		String chars = "[\\s\'\";]";
		return syntax.getSchema().replaceAll(chars, "");
	}

}
