package dataService.queryBuilder;

import java.util.ArrayList;

import dataService.syntaxComposer.DdlSyntaxComposer;
import dataService.syntaxComposer.SqlColumn;

public class CreateTableQueryBuilder extends QueryBuilder {

	private DdlSyntaxComposer syntax;
	private ArrayList<SqlColumn> primaryKeys;
	private ArrayList<SqlColumn> foreignKeys;

	public CreateTableQueryBuilder(DdlSyntaxComposer syntax) {
		this.primaryKeys = new ArrayList<>();
		this.foreignKeys = new ArrayList<>();
		this.syntax = syntax;
	}

	@Override
	public void buildString() {
		// TODO split this into several subfunctions, if possible without too many
		// parameters (these obstruct readability)
		
		// FIXME Foreign keys not implemented - do this!

		// Table definition
		builder.append("CREATE TABLE ");
		if (syntax.getSchema() != null) {
			builder.append(syntax.getSchema());
			builder.append(".");
		}
		builder.append(syntax.getTable());
		builder.append(" (\n");

		// Column definition
		for (int i = 0; i < syntax.getCols().size(); i++) {
			SqlColumn col = syntax.getCols().get(i);
			builder.append(col.getName());
			builder.append(" ");
			builder.append(col.getDataType());
			if (col.getDataTypeLength() > 0) {
				builder.append("(");
				builder.append(col.getDataTypeLength());
				builder.append(")");
			}
			if (col.isNotNull()) {
				builder.append(" NOT NULL");
			}
			if (col.getDefaultValue() != null) {
				builder.append(col.getDefaultValue());
			}
			if (col.isAutoincrement()) {
				builder.append(" AUTO_INCREMENT");
			}
			if (col.isPrimaryKey()) {
				this.primaryKeys.add(col);
			}
			if (col.isForeignKey()) {
				this.foreignKeys.add(col);
			}

			if (i < syntax.getCols().size() - 1) {
				builder.append(",\n");
			} else {

				// Constraint definition - foreign keys
				if (!this.foreignKeys.isEmpty()) {
					builder.append(",\n");
					for (int j = 0; j < foreignKeys.size(); j++) {
						builder.append("FOREIGN KEY (");
						builder.append(foreignKeys.get(j).getName());
						builder.append(")\nREFERENCES ");
						builder.append(foreignKeys.get(j).getForeignKeyDestination().getKey());
						builder.append(" (");
						builder.append(foreignKeys.get(j).getForeignKeyDestination().getValue());
						builder.append(") \n");
						if (foreignKeys.get(j).getOnDelete() != null) {
							builder.append("ON DELETE ");
							builder.append(foreignKeys.get(j).getOnDelete());
						}
						if (foreignKeys.get(j).getOnUpdate() != null) {
							builder.append(" ON UPDATE ");
							builder.append(foreignKeys.get(j).getOnUpdate());
						}
					}
				}

				// Constraint definition - primary key -- end
				builder.append(",\nPRIMARY KEY (");
				for (int k = 0; k < this.primaryKeys.size(); k++) {
					builder.append(primaryKeys.get(k).getName());
					if (k == primaryKeys.size() - 1) {
						builder.append(")\n);");
					} else {
						builder.append(", ");
					}
				}
			}
		}
		builder.append(";");

		this.finalQuery = builder.toString();
	}

}
