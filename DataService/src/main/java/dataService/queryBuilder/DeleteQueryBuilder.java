package dataService.queryBuilder;

import dataService.syntaxComposer.DmlSyntaxComposer;

public class DeleteQueryBuilder extends QueryBuilder {

	private DmlSyntaxComposer syntax;
	public DeleteQueryBuilder(DmlSyntaxComposer syntax) {
		this.syntax = syntax;
	}

	@Override
	public void buildString() {
		builder.append("DELETE FROM ");
		if(syntax.getSchema() != null) {
			builder.append(syntax.getSchema());
			builder.append(".");
		}
		builder.append(syntax.getTable());
		builder.append(" WHERE ");
		splitFilter(syntax.getFilter());

		this.finalQuery = builder.toString();
		System.out.println("Built following SQL query: " + finalQuery);
	}

}
