package dataService.queryBuilder;

import java.util.ArrayList;

import dataService.syntaxComposer.DmlSyntaxComposer;
import dataService.syntaxComposer.SqlColumn;

public class InsertQueryBuilder extends QueryBuilder {

	private DmlSyntaxComposer syntax;

	public InsertQueryBuilder(DmlSyntaxComposer syntax) {
		this.syntax = syntax;
	}

	@Override
	public void buildString() {
		builder.append("INSERT INTO ");
		if (this.syntax.getSchema() != null) {
			builder.append(this.syntax.getSchema());
			builder.append(".");
		}
		builder.append(this.syntax.getTable());
		// cols can be empty for whole-table-updates
		if (!this.syntax.getCols().isEmpty()) {
			ArrayList<SqlColumn> cols = this.syntax.getCols();
			builder.append("(");
			for (int i = 0; i < cols.size(); i++) {
				builder.append(cols.get(i).getName());
				if (i == cols.size() - 1) {
					builder.append(")\n");
				} else {
					builder.append(", ");
				}
			}
		}
		builder.append(" VALUES (");
		for (int i = 0; i < this.syntax.getCols().size(); i++) {
			builder.append("?");
			if (i == this.syntax.getCols().size() - 1) {
				builder.append(")");
			} else {
				builder.append(", ");
			}
		}
		builder.append(";");

		finalQuery = builder.toString();
		System.out.println("Built following SQL query: " + finalQuery);
	}

}
