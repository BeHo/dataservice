package dataService.queryBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class QueryBuilder {

	protected StringBuilder builder;
	protected String finalQuery;
	protected ArrayList<String> allValues;

	public QueryBuilder() {
		this.builder = new StringBuilder();
		this.allValues = new ArrayList<>();
	}

	public StringBuilder getBuilder() {
		return builder;
	}

	public void setBuilder(StringBuilder builder) {
		this.builder = builder;
	}

	public String getFinalQuery() {
		return finalQuery;
	}

	public void setFinalQuery(String finalQuery) {
		this.finalQuery = finalQuery;
	}

	public ArrayList<String> getValues() {
		return allValues;
	}

	/**
	 * Implementations of this method should use the class' <i>builder</i> attribute
	 * to compose a basic SQL query string. More specific tasks in this context
	 * should be done in separate, subclass specific methods. <b>IMPORTANT!!!</b> It
	 * is crucial to <i>ALWAYS</i> set the 'finalQuery' field at the end of this
	 * method! Otherwise, all attempts to execute any statement built on this
	 * method's query string will fail due to null pointers!
	 */
	public abstract void buildString();

	/**
	 * In case a filter clause exists, it can be passed to this method to split it
	 * up and replace concrete values by SQL placeholders (?) in order to prevent
	 * SQL injection and make the whole query usable with prepared statements. The
	 * method requires the filter clause and operates directly on the current
	 * instance's <i>builder<i/> attribute. Extracted/replaced values are used to
	 * fill the String[] (a class attribute) 'values'.
	 * 
	 * @param filter
	 *            - String. A filter/WHERE clause matching general SQL syntax rules.
	 */
	protected void splitFilter(String filter) {

		// Split the filter string into several separate conditions (if any)
		ArrayList<String> splitFilter = new ArrayList<>(Arrays.asList((filter.split("\\sAND\\s"))));

		// Build regex searching for SQL operators and the subsequent value.
		// Reason: we need to split and replace the value by placeholders, but also keep
		// the delimiters and the replaced value. This is not possible using
		// String.split().
		Pattern regEx = Pattern.compile("([<>=]=?|\\s(?:NOT)? IN\\s)(.*)");

		// Split each condition, replace the value by "(?)", store the previous value.
		for (int i = 0; i < splitFilter.size(); i++) {
			String pattern = splitFilter.get(i);
			Matcher matcher = regEx.matcher(pattern);
			if (matcher.find()) {
				allValues.add(matcher.group(2).trim());
				pattern = pattern.replace(matcher.group(2), " (?)");
				builder.append(pattern);
				// Omit AND at the end
				if (i != splitFilter.size() - 1) {
					builder.append(" AND ");
				}
			}
		}
		builder.append(";");
	}
}
