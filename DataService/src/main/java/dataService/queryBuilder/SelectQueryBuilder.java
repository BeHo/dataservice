package dataService.queryBuilder;

import java.util.ArrayList;

import dataService.syntaxComposer.DqlSyntaxComposer;
import dataService.syntaxComposer.SqlColumn;

public class SelectQueryBuilder extends QueryBuilder {

	private DqlSyntaxComposer syntax;

	public SelectQueryBuilder(DqlSyntaxComposer syntax) {
		this.syntax = syntax; // needs table, filter, cols
	}

	@Override
	public void buildString() {
		ArrayList<SqlColumn> cols = this.syntax.getCols();
		builder.append("SELECT ");
		if (cols != null) {
			for (int i = 0; i < cols.size(); i++) {
				builder.append(cols.get(i).getName());
				if (i != cols.size() - 1) {
					builder.append(", ");
				}
			}
		} else if (syntax.getMaxValue() != null) {
			builder.append("MAX(");
			builder.append(syntax.getMaxValue());
			builder.append(")");
		} else if (syntax.getMinValue() != null) {
			builder.append("MIN(");
			builder.append(syntax.getMinValue());
			builder.append(")");
		} else {
			builder.append("*");
		}
		builder.append(" FROM ");
		if (this.syntax.getSchema() != null) {
			builder.append(this.syntax.getSchema());
			builder.append(".");
		}
		builder.append(syntax.getTable());
		if (this.syntax.getFilter() != null) {
			builder.append(" WHERE ");
			splitFilter(this.syntax.getFilter());
		}
		this.finalQuery = builder.toString();

		System.out.println("Built following SQL query: " + finalQuery);
	}

}
