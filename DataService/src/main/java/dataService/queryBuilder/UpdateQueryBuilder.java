package dataService.queryBuilder;

import java.util.ArrayList;

import dataService.syntaxComposer.DmlSyntaxComposer;
import dataService.syntaxComposer.SqlColumn;

public class UpdateQueryBuilder extends QueryBuilder {

	private DmlSyntaxComposer syntax;

	public UpdateQueryBuilder(DmlSyntaxComposer syntax) {
		this.syntax = syntax;
	}

	@Override
	public void buildString() {
		builder.append("UPDATE ");
		if (syntax.getSchema() != null) {
			builder.append(syntax.getSchema());
			builder.append(".");
		}
		builder.append(syntax.getTable());
		builder.append(" SET \n");
		setColAssignments();
		if (syntax.getFilter() != null || !"".equals(syntax.getFilter())) {
			builder.append(" WHERE ");
			splitFilter(syntax.getFilter());
		}
		this.finalQuery = builder.toString();
		System.out.println("Built following SQL query: " + finalQuery);
	}

	private void setColAssignments() {
		ArrayList<SqlColumn> cols = syntax.getCols();
		for (int i = 0; i < cols.size(); i++) {
			this.allValues.add(cols.get(i).getNewValue());
			builder.append(cols.get(i));
			builder.append(" = ?");
			if (i != cols.size() - 1) {
				builder.append(", \n");
			} else {
				builder.append("\n");
			}
		}
	}

}
