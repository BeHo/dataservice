package dataService.statementExecuter;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import dataService.queryBuilder.QueryBuilder;

public class CreateStatementExecuter {

	private Connection conn;
	private QueryBuilder queryBuilder;

	public CreateStatementExecuter(Connection conn, QueryBuilder queryBuilder) {
		super();
		this.conn = conn;
		this.queryBuilder = queryBuilder;
	}

	public void executeStatement() {
		try {
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(queryBuilder.getFinalQuery());
			System.out.println("DDL statement executed successfully: " + queryBuilder.getFinalQuery());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
