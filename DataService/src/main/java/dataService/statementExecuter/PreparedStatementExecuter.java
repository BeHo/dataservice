package dataService.statementExecuter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;

import dataService.config.JdbcDataTypes;
import dataService.queryBuilder.QueryBuilder;

/**
 * Superclass for execution classes of SQL prepared statements. Most of the
 * actual logic is stored here tht is in <i>buildStatement()</i> which must be
 * called first. Subclasses are meant to address the difference between JDBC's
 * executeUpdate() and executeQuery() methods, the first being used for
 * insert/update/delete requests, the latter for select statements. Therefore,
 * all that has to be done when extending this class is overriding
 * <i>executeStatement()</i>, thereby implementing the correct JDBC method call.
 * 
 * @author Bene
 *
 */
public abstract class PreparedStatementExecuter {

	protected PreparedStatement stmt;
	protected QueryBuilder queryBuilder;
	protected JdbcDataTypes types[];
	protected Connection conn;

	public PreparedStatementExecuter(Connection connection, QueryBuilder queryBuilder) {
		this.queryBuilder = queryBuilder;
		this.conn = connection;
		this.types = null;
	}

	public JdbcDataTypes[] getTypes() {
		return types;
	}

	public void setTypes(JdbcDataTypes[] types) {
		this.types = types;
	}

	public final void buildStatement() {
		try {
			this.stmt = conn.prepareStatement(queryBuilder.getFinalQuery(), Statement.RETURN_GENERATED_KEYS);

			// return if this is a statement without placeholders (e.g. SELECT * FROM table)
			if (this.types == null) {
				return;
			}

			// Otherwise, set the placeholders.
			// NOTE: boolean is not supported by all DB systems, so it is treated as CHAR(1)
			// requiring a fixed call of setString()
			for (int i = 0; i < types.length; i++) {
				String type = types[i].toString();
				String setterName;
				if ("Boolean".equals(type)) {
					setterName = "set" + "String";
				} else {
					setterName = "set" + type;
				}
				Method stmtSetter = PreparedStatement.class.getDeclaredMethod(setterName, int.class,
						getClassFromTypesEnum(type));
				invokeSetterMethod(stmt, i, stmtSetter);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void invokeSetterMethod(PreparedStatement stmt, int i, Method stmtSetter)
			throws IllegalAccessException, InvocationTargetException, ClassNotFoundException {
		switch (types[i]) {
		case String:
			stmtSetter.invoke(stmt, i + 1, queryBuilder.getValues().get(i));
		case Int:
			stmtSetter.invoke(stmt, i + 1, Integer.parseInt(queryBuilder.getValues().get(i)));
			break;
		case Long:
			stmtSetter.invoke(stmt, i + 1, Long.parseLong(queryBuilder.getValues().get(i)));
			break;
		case Double:
			stmtSetter.invoke(stmt, i + 1, Double.parseDouble(queryBuilder.getValues().get(i)));
			break;
		case Float:
			stmtSetter.invoke(stmt, i + 1, Float.parseFloat(queryBuilder.getValues().get(i)));
		case Date:
			stmtSetter.invoke(stmt, i + 1, Date.valueOf(queryBuilder.getValues().get(i)));
			break;
		case Datetime:
			stmtSetter.invoke(stmt, i + 1, Timestamp.valueOf(queryBuilder.getValues().get(i)));
			break;
		case Timestamp:
			stmtSetter.invoke(stmt, i + 1, Timestamp.valueOf(queryBuilder.getValues().get(i)));
			break;
		case Boolean:
			boolean boolValue = Boolean.valueOf(queryBuilder.getValues().get(i));
			if (boolValue == true) {
				stmtSetter.invoke(stmt, i + 1, "1");
			} else {
				stmtSetter.invoke(stmt, i + 1, "0");
			}
			break;
		default:
			System.out.println(
					"No supported SQL data type found for placeholder replacement in Prepared Statement. Returning...");
		}
		// Column index of set... methods starts at 1
	}

	protected Class<?> getClassFromTypesEnum(String type) throws ClassNotFoundException {
		Class<?> rv = null;
		// TODO Use switch for stuff below???
		if ("Int".equals(type)) {
			rv = int.class;
		} else if ("Double".equals(type)) {
			rv = double.class;
		} else if ("Float".equals(type)) {
			rv = float.class;
		} else if ("Long".equals(type)) {
			rv = long.class;
			// Lines below are needed because there's now way to get the Class<?> from
			// String other then Class.forName(), which requires fully qualified class
			// names. These can't be retrieved dynamically.
		} else if ("String".equals(type)) {
			rv = Class.forName("java.lang.String");
		} else if ("Date".equals(type)) {
			rv = Class.forName("java.sql.Date");
		} else if ("Datetime".equals(type) | "Timestamp".equals(type)) {
			rv = Class.forName("java.sql.Timestamp");
		} else {
			System.out.println("Unsupported data type.");
			throw new ClassNotFoundException();
		}
		return rv;
	}

	/**
	 * Implementations of this class should set the respective outcome (ResultSet or
	 * int representing the inserted row's ID) as attributes of the classes
	 * extending this class. These attributes have to be added for each subclass
	 * accordingly
	 */
	public abstract void executeStatement();
}
