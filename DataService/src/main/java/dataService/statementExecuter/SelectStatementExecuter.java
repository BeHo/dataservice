package dataService.statementExecuter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dataService.ResultSetMapper;
import dataService.queryBuilder.QueryBuilder;

public class SelectStatementExecuter extends PreparedStatementExecuter {

	private Object mapperObject;
	private ArrayList<Object> results;

	public SelectStatementExecuter(Connection conn, QueryBuilder queryBuilder, Object mapperObject) {
		super(conn, queryBuilder);
		this.mapperObject = mapperObject;
		// TODO Auto-generated constructor stub
	}

	public ArrayList<Object> getResults() {
		return results;
	}

	@Override
	public void executeStatement() {
		ResultSet resSet = null;
		try {
			resSet = stmt.executeQuery();
			ResultSetMapper mapper = new ResultSetMapper(resSet, mapperObject);
			this.results = mapper.mapToObject();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
