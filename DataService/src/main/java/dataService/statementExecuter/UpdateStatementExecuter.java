package dataService.statementExecuter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dataService.queryBuilder.QueryBuilder;

public class UpdateStatementExecuter extends PreparedStatementExecuter {

	private int affectedRows;
	private ArrayList<Integer> insertedIds;

	public UpdateStatementExecuter(Connection conn, QueryBuilder queryBuilder) {
		super(conn, queryBuilder);
	}

	public int getAffectedRows() {
		return affectedRows;
	}

	public ArrayList<Integer> getInsertedIds() {
		return insertedIds;
	}

	@Override
	public void executeStatement() {
		try {
			affectedRows = stmt.executeUpdate();
			ResultSet genKeys = stmt.getGeneratedKeys();
			if (!genKeys.isBeforeFirst()) {
				System.out.println(
						"No data in result set, so probably no insert statement or incorrect database interaction.");
			} else {
				insertedIds = new ArrayList<>();
				while (genKeys.next()) {
					System.out.println("Inserted row at ID " + genKeys.getInt(1));
					insertedIds.add(genKeys.getInt(1));
				}
			}
			System.out.println("Processed " + affectedRows + " row(s) in insert/update/delete request.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
