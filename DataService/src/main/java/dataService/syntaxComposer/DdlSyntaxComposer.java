package dataService.syntaxComposer;

import dataService.config.DbType;

/**
 * This subclass of {@link SqlSyntaxComposer} should be used to provide the most
 * commonly used syntax tokens for DDL statements including CREATE TABLE, CREATE
 * SCHEMA. QueryBuilder classes will then use these tokens to build up a valid
 * SQL string that can be executed later. Note that not all options and
 * parameters for every DDL statement are supported, and support is limited to
 * the database engines included in this package (MySQL, Postgre, H2, SQLite)
 * 
 * @author hotz
 *
 */
public class DdlSyntaxComposer extends SqlSyntaxComposer {

	private static final long serialVersionUID = -989496943940929605L;
	private String user;
	private DbType dbType;

	public String getUser() {
		return user;
	}

	public DbType getDbType() {
		return dbType;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setDbType(DbType dbType) {
		this.dbType = dbType;
	}

}
