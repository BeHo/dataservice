package dataService.syntaxComposer;

public class DmlSyntaxComposer extends SqlSyntaxComposer {

	private static final long serialVersionUID = -7625895154179909926L;
	private String filter;

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

}
