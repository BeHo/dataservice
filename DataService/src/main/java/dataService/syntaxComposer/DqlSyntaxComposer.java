package dataService.syntaxComposer;

public class DqlSyntaxComposer extends SqlSyntaxComposer {

	private static final long serialVersionUID = -8993899292887680090L;
	private String filter;
	private String maxValue;
	private String minValue;

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getMaxValue() {
		return maxValue;
	}

	/**
	 * Set the value (column, table, expression) to be used in the MAX() function.
	 * Scope is the SELECT statement, not WHERE or HAVING clauses.
	 * 
	 * @param maxValue
	 */
	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public String getMinValue() {
		return minValue;
	}

	public void setMinValue(String minValue) {
		this.minValue = minValue;
	}
}
