package dataService.syntaxComposer;

import dataService.config.SqlDataTypes;
import dataService.config.SqlOnDeleteOrUpdateActions;
import javafx.util.Pair;

public final class SqlColumn {

	private String name;
	private SqlDataTypes dataType;
	private int dataTypeLength;
	private boolean notNull;
	private String defaultValue;
	private String newValue;
	private boolean autoincrement;
	private boolean isPrimaryKey;
	private boolean isForeignKey;
	private Pair<String, String> foreignKeyDestination;
	private SqlOnDeleteOrUpdateActions onDelete;
	private SqlOnDeleteOrUpdateActions onUpdate;

	public SqlColumn(String name) {
		this.name = name;
		this.dataTypeLength = 0;
	}

	public SqlOnDeleteOrUpdateActions getOnDelete() {
		return onDelete;
	}

	public SqlOnDeleteOrUpdateActions getOnUpdate() {
		return onUpdate;
	}

	public void setOnDelete(SqlOnDeleteOrUpdateActions onDelete) {
		this.onDelete = onDelete;
	}

	public void setOnUpdate(SqlOnDeleteOrUpdateActions onUpdate) {
		this.onUpdate = onUpdate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SqlDataTypes getDataType() {
		return dataType;
	}

	public void setDataType(SqlDataTypes dataType) {
		this.dataType = dataType;
	}

	public int getDataTypeLength() {
		return dataTypeLength;
	}

	public void setDataTypeLength(int dataTypeLength) {
		this.dataTypeLength = dataTypeLength;
	}

	public boolean isNotNull() {
		return notNull;
	}

	public void setNotNull(boolean notNull) {
		this.notNull = notNull;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	public boolean isAutoincrement() {
		return autoincrement;
	}

	public void setAutoincrement(boolean autoincrement) {
		this.autoincrement = autoincrement;
	}

	public boolean isPrimaryKey() {
		return isPrimaryKey;
	}

	public void setPrimaryKey(boolean isPrimaryKey) {
		this.isPrimaryKey = isPrimaryKey;
	}

	public boolean isForeignKey() {
		return isForeignKey;
	}

	public void setForeignKey(boolean isForeignKey) {
		this.isForeignKey = isForeignKey;
	}

	public Pair<String, String> getForeignKeyDestination() {
		return foreignKeyDestination;
	}

	/**
	 * Set the column (=value) and table (=key) referenced by this foreign key. 
	 * @param foreignKeyDestination
	 */
	public void setForeignKeyDestination(Pair<String, String> foreignKeyDestination) {
		this.foreignKeyDestination = foreignKeyDestination;
	}

}
