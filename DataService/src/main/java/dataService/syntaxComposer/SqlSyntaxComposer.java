package dataService.syntaxComposer;

import java.io.Serializable;
import java.util.ArrayList;

import dataService.config.JdbcDataTypes;

/**
 * Abstract superclass for DDL-, DML- and DQL syntax composers. As a
 * Javabean-like class, it provides just fields and their respective getters and
 * setters (as should any extending subclass!). In this particular case,
 * SqlSyntaxComposer contains all the basic stuff such as table and schema name,
 * an Array of {@link JdbcDataTypes} etc.
 * 
 * @author hotz
 *
 */
public abstract class SqlSyntaxComposer implements Serializable {

	private static final long serialVersionUID = 6924439860736592899L;
	private String schema;
	private String table;
	private ArrayList<SqlColumn> cols;

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getTable() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public ArrayList<SqlColumn> getCols() {
		return cols;
	}

	/*
	 * Add a column to the array. Note that this method will autmatically
	 * instantiate the column array if doesn't exist yet.
	 */
	public void addCol(SqlColumn col) {
		if (this.cols == null) {
			this.cols = new ArrayList<>();
		}
		this.cols.add(col);
	}
}
